module FilesLoadingHelper
  def upload_file(file_info, save_path, file_save_name = file_info[:filename]) #raise NoMethodError if choose nothing
    Dir.mkdir(save_path) unless Dir.exist?(save_path) #raise SystemCallError
    File.open(save_path + file_save_name, "w") do |f|
      f.write(file_info[:tempfile].read)
    end
  end

  def file_read(file_path)
    File.open(file_path, "r") do |f|
      f.readlines
    end
  end

  def file_save(file_path, lines)
    File.open(file_path, "w") do |f|
      lines.each { |line| f.write line }
    end
  end


end