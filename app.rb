 'rubygems'
require 'sinatra'
require 'sinatra/base'
require 'tilt/haml'
require_relative 'helpers/files_loading_helper'

class App < Sinatra::Base
  set :haml, :format => :html5
  set :haml, :layout => :layout

  helpers FilesLoadingHelper

  def initialize
    super
    @headInfo = Hash.new
    @file = 0
  end

  get '/' do
    @headInfo[:title] = 'aaa'
    haml :favicon_upload
  end

  post '/upload' do
    upload_file(params['myfile'], 'public/uploads/')
    "The file was successfully uploaded!"
  end

  get '/:page_name' do
    begin
      page = haml @params[:page_name].to_sym
    rescue SystemCallError
      raise not_found
    end
  end

  not_found do
    haml 'This is nowhere to be found.'
  end
end